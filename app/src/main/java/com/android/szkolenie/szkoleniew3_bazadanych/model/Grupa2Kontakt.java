package com.android.szkolenie.szkoleniew3_bazadanych.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by papcio28 on 10.05.2015.
 */
@DatabaseTable(tableName = "grupa2kontakt")
public class Grupa2Kontakt {
    @DatabaseField(generatedId = true, columnName = "id")
    private int mID;

    @DatabaseField(foreign = true)
    private Kontakt mKontakt;

    @DatabaseField(foreign = true, foreignAutoRefresh = true) // foreignAutoRefresh - aby pobieraly sie szczegoly grup do listy
    private Grupa mGrupa;

    public int getmID() {
        return mID;
    }

    public void setmID(int mID) {
        this.mID = mID;
    }

    public Kontakt getmKontakt() {
        return mKontakt;
    }

    public void setmKontakt(Kontakt mKontakt) {
        this.mKontakt = mKontakt;
    }

    public Grupa getmGrupa() {
        return mGrupa;
    }

    public void setmGrupa(Grupa mGrupa) {
        this.mGrupa = mGrupa;
    }
}
