package com.android.szkolenie.szkoleniew3_bazadanych;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.android.szkolenie.szkoleniew3_bazadanych.model.Grupa;
import com.android.szkolenie.szkoleniew3_bazadanych.model.Grupa2Kontakt;
import com.android.szkolenie.szkoleniew3_bazadanych.model.Kontakt;
import com.android.szkolenie.szkoleniew3_bazadanych.model.Telefon;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by papcio28 on 10.05.2015.
 */
public class KontaktyOpenHelper extends OrmLiteSqliteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "kontakty.db";

    public KontaktyOpenHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    // Jest wywolywana kiedy baza danych nie istnieje
    // Tutaj tworzymy strukture bazy danych, ewentualnie dodajemy dane poczatkowego
    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {

            // Tworzymy struktury tabeli w bazie danych
            TableUtils.createTable(connectionSource, Grupa.class);
            TableUtils.createTable(connectionSource, Kontakt.class);
            TableUtils.createTable(connectionSource, Telefon.class);
            TableUtils.createTable(connectionSource, Grupa2Kontakt.class);

            // Obiekt dostępu do danych w tabeli Grupa
            Dao<Grupa, Integer> mGrupaDAO = DaoManager.createDao(connectionSource, Grupa.class);

            // Dodajemy przykładowe grupy do bazy danych w momencie jej tworzenia
            mGrupaDAO.create(new Grupa("Rodzina"));
            mGrupaDAO.create(new Grupa("Znajomi"));
            mGrupaDAO.create(new Grupa("Partnerzy biznesowi"));

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Implementacja zmiany struktury z wersji na wersje
    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }
}
