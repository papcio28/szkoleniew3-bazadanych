package com.android.szkolenie.szkoleniew3_bazadanych;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.szkolenie.szkoleniew3_bazadanych.model.Grupa2Kontakt;
import com.android.szkolenie.szkoleniew3_bazadanych.model.Kontakt;

import java.util.List;

/**
 * Created by papcio28 on 10.05.2015.
 */
public class KontaktyAdapter extends ArrayAdapter<Kontakt> {
    private final LayoutInflater mInflater;

    public KontaktyAdapter(Context context, List<Kontakt> objects) {
        super(context, R.layout.list_item_kontakt, objects);
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View mWiersz = mInflater.inflate(R.layout.list_item_kontakt, null);

        Kontakt mItem = getItem(position);

        TextView mNazwa = (TextView) mWiersz.findViewById(R.id.kontakt_nazwa);
        TextView mGrupy = (TextView) mWiersz.findViewById(R.id.kontakt_grupy);

        mNazwa.setText(mItem.getmImie() + " " + mItem.getmNazwisko());

        StringBuilder mGrupyText = new StringBuilder();
        for(Grupa2Kontakt mPowiazanie : mItem.getmGrupy()) {
            mGrupyText.append(mPowiazanie.getmGrupa().getmNazwa()).append(", ");
        }
        // Usuniecnie ostatniego przecinka
        if(mGrupyText.indexOf(", ") >= 0) {
            mGrupyText.delete(mGrupyText.lastIndexOf(", "), mGrupyText.length());
        }

        mGrupy.setText(mGrupyText.toString());

        return mWiersz;
    }
}
