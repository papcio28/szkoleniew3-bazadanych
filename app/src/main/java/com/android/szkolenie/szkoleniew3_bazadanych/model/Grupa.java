package com.android.szkolenie.szkoleniew3_bazadanych.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by papcio28 on 10.05.2015.
 */
@DatabaseTable(tableName = "grupa")
public class Grupa {
    @DatabaseField(generatedId = true, columnName = "id")
    private int mID;

    // Unikalne pole, niepuste (NOT NULL)
    @DatabaseField(columnName = "nazwa", unique = true, canBeNull = false)
    private String mNazwa;

    @ForeignCollectionField
    private ForeignCollection<Grupa2Kontakt> mKontakty;

    public Grupa() {

    }

    public Grupa(String mNazwa) {
        this.mNazwa = mNazwa;
    }

    public int getmID() {
        return mID;
    }

    public void setmID(int mID) {
        this.mID = mID;
    }

    public String getmNazwa() {
        return mNazwa;
    }

    public void setmNazwa(String mNazwa) {
        this.mNazwa = mNazwa;
    }

    public ForeignCollection<Grupa2Kontakt> getmKontakty() {
        return mKontakty;
    }

    public void setmKontakty(ForeignCollection<Grupa2Kontakt> mKontakty) {
        this.mKontakty = mKontakty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Grupa grupa = (Grupa) o;

        if (mID != grupa.mID) return false;
        if (!mNazwa.equals(grupa.mNazwa)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = mID;
        result = 31 * result + mNazwa.hashCode();
        return result;
    }
}
