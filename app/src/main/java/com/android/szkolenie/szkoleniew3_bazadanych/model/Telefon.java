package com.android.szkolenie.szkoleniew3_bazadanych.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by papcio28 on 10.05.2015.
 */
@DatabaseTable(tableName = "telefon")   // Tabela bazy danych
public class Telefon {
    // Kolumna bazy danych z automatycznie inkrementowanym ID
    @DatabaseField(generatedId = true, columnName = "id")
    private int mID;

    @DatabaseField(columnName = "rodzaj")   // Kolumna bazy danych
    private String mRodzaj;

    @DatabaseField(columnName = "numer_telefonu")
    private String mNumerTelefonu;

    @DatabaseField(foreign = true)
    private Kontakt mKontakt;   // Określenie relacji z tabelą Kontakt

    public Telefon() {}

    public Telefon(String mRodzaj, String mNumer) {
        this.mRodzaj = mRodzaj;
        this.mNumerTelefonu = mNumer;
    }
    // Jeden obiekt telefonu ma związek z jednym obiektem typu Kontakt

    public int getmID() {
        return mID;
    }

    public void setmID(int mID) {
        this.mID = mID;
    }

    public String getmRodzaj() {
        return mRodzaj;
    }

    public void setmRodzaj(String mRodzaj) {
        this.mRodzaj = mRodzaj;
    }

    public String getmNumerTelefonu() {
        return mNumerTelefonu;
    }

    public void setmNumerTelefonu(String mNumerTelefonu) {
        this.mNumerTelefonu = mNumerTelefonu;
    }

    public Kontakt getmKontakt() {
        return mKontakt;
    }

    public void setmKontakt(Kontakt mKontakt) {
        this.mKontakt = mKontakt;
    }
}
