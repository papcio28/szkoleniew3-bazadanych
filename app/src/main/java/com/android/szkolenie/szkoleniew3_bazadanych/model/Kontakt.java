package com.android.szkolenie.szkoleniew3_bazadanych.model;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * Created by papcio28 on 10.05.2015.
 */
@DatabaseTable(tableName = "kontakt")
public class Kontakt {
    @DatabaseField(generatedId = true, columnName = "id")
    private int mID;

    // Lista powiązanych obiektów typu telefon
    @ForeignCollectionField(eager = true)
    private ForeignCollection<Telefon> mNumery;

    @ForeignCollectionField(maxEagerForeignCollectionLevel = 3, maxEagerLevel = 3, eager = true)
    private ForeignCollection<Grupa2Kontakt> mGrupy;

    @DatabaseField(columnName = "imie")
    private String mImie;

    @DatabaseField(columnName = "nazwisko")
    private String mNazwisko;

    @DatabaseField(columnName = "wiek")
    private Integer mWiek = 0;

    public int getmID() {
        return mID;
    }

    public void setmID(int mID) {
        this.mID = mID;
    }

    public ForeignCollection<Telefon> getmNumery() {
        return mNumery;
    }

    public void setmNumery(ForeignCollection<Telefon> mNumery) {
        this.mNumery = mNumery;
    }

    public String getmImie() {
        return mImie;
    }

    public void setmImie(String mImie) {
        this.mImie = mImie;
    }

    public String getmNazwisko() {
        return mNazwisko;
    }

    public void setmNazwisko(String mNazwisko) {
        this.mNazwisko = mNazwisko;
    }

    public Integer getmWiek() {
        return mWiek;
    }

    public void setmWiek(Integer mWiek) {
        this.mWiek = mWiek;
    }

    public ForeignCollection<Grupa2Kontakt> getmGrupy() {
        return mGrupy;
    }

    public void setmGrupy(ForeignCollection<Grupa2Kontakt> mGrupy) {
        this.mGrupy = mGrupy;
    }
}
