package com.android.szkolenie.szkoleniew3_bazadanych;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.szkolenie.szkoleniew3_bazadanych.model.Grupa;
import com.android.szkolenie.szkoleniew3_bazadanych.model.Grupa2Kontakt;
import com.android.szkolenie.szkoleniew3_bazadanych.model.Kontakt;
import com.android.szkolenie.szkoleniew3_bazadanych.model.Telefon;
import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by papcio28 on 10.05.2015.
 */
public class KontaktFormActivity extends ActionBarActivity {

    @InjectView(R.id.form_imie)
    protected EditText mImie;
    @InjectView(R.id.form_nazwisko)
    protected EditText mNazwisko;
    @InjectView(R.id.form_wiek)
    protected EditText mWiek;

    @InjectView(R.id.numer_rodzaj)
    protected EditText mNumerRodzaj;
    @InjectView(R.id.numer_telefonu)
    protected EditText mNumerTelefonu;

    @InjectView(R.id.grupy_container)
    protected LinearLayout mGrupy;
    @InjectView(R.id.numery_container)
    protected LinearLayout mTelefony;

    protected Kontakt mKontakt;

    // Pola do obslugi DB
    private ConnectionSource mConnection;
    private Dao<Kontakt, Integer> mDAO;
    private Dao<Telefon, Integer> mNumeryDAO;
    private Dao<Grupa2Kontakt, Integer> mPowiazanieDAO;
    private Dao<Grupa, Integer> mGrupaDAO;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kontakt_form);
        ButterKnife.inject(this);

        mConnection = new AndroidConnectionSource(new KontaktyOpenHelper(this));
        try {
            mDAO = DaoManager.createDao(mConnection, Kontakt.class);
            mNumeryDAO = DaoManager.createDao(mConnection, Telefon.class);
            mPowiazanieDAO = DaoManager.createDao(mConnection, Grupa2Kontakt.class);
            mGrupaDAO = DaoManager.createDao(mConnection, Grupa.class);
        } catch (SQLException e) {
            e.printStackTrace();
            finish();
            return;
        }

        // Sprawdzamy czy podano parametr z ID kontaktu
        if(getIntent().hasExtra(MainActivity.KONTAKT_EXTRA)) {
            int kontaktID = getIntent().getIntExtra(MainActivity.KONTAKT_EXTRA, 0);
            try {
                // Probujemy wyszykac kontakt po podanym ID
                mKontakt = mDAO.queryForId(kontaktID);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        // Jezeli nie podano ID lub nie udalo sie znalezc w DB to utworz nowy wpis
        if(mKontakt == null) {
            mKontakt = new Kontakt();
        }

        refreshForm();
    }

    public void refreshForm() {
        mImie.setText(mKontakt.getmImie());
        mNazwisko.setText(mKontakt.getmNazwisko());
        mWiek.setText(mKontakt.getmWiek() + "");

        // Numery telefonow
        refreshTelefony();

        // Grupy
        try {
            List<Grupa> mAllGroups = mGrupaDAO.queryForAll();
            mGrupy.removeAllViews();
            for(Grupa mGrupa : mAllGroups) {
                CheckBox mGrupaBox = new CheckBox(this);
                mGrupaBox.setText(mGrupa.getmNazwa());
                if(mKontakt.getmGrupy() != null) {
                    for(Grupa2Kontakt mPowiazanie : mKontakt.getmGrupy()) {
                        if(mPowiazanie.getmGrupa().equals(mGrupa)) {
                            mGrupaBox.setChecked(true);
                        }
                    }
                }
                mGrupaBox.setTag(mGrupa);
                mGrupy.addView(mGrupaBox);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void refreshTelefony() {
        if(mKontakt.getmNumery() != null) {
            mTelefony.removeAllViews();
            for (Telefon mTelefon : mKontakt.getmNumery()) {
                TextView mNumerLabel = new TextView(this);
                mNumerLabel.setText(mTelefon.getmRodzaj()+": "+mTelefon.getmNumerTelefonu());
                mTelefony.addView(mNumerLabel);
            }
        }
    }

    @OnClick(R.id.btn_dodaj_numer)
    public void nowyNumer() {
        String mRodzaj = mNumerRodzaj.getText().toString();
        String mNumer = mNumerTelefonu.getText().toString();
        if(mKontakt.getmNumery() == null) {
            try {
                mDAO.assignEmptyForeignCollection(mKontakt, "mNumery");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        mKontakt.getmNumery().add(new Telefon(mRodzaj, mNumer));

        refreshTelefony();
    }

    @OnClick(R.id.btn_zapisz)
    public void nowyKontakt() {
        mKontakt.setmImie(mImie.getText().toString());
        mKontakt.setmNazwisko(mNazwisko.getText().toString());
        mKontakt.setmWiek(Integer.parseInt(mWiek.getText().toString()));

        try {
            mDAO.createOrUpdate(mKontakt);

            // Telefony ??
            for(Telefon mTelefon : mKontakt.getmNumery()) {
                mTelefon.setmKontakt(mKontakt);
                mNumeryDAO.createOrUpdate(mTelefon);
            }

            // Grupy
            for(int i = 0; i < mGrupy.getChildCount(); i++) {
                // Usuniecie bieżących powiązań
                mPowiazanieDAO.delete(mKontakt.getmGrupy());

                // Dodanie nowych powiązań z zaznaczonych CheckBox'ów
                CheckBox mCheckBox = (CheckBox) mGrupy.getChildAt(i);
                if(mCheckBox.isChecked()) {
                    Grupa mGrupaTag = (Grupa) mCheckBox.getTag();
                    Grupa2Kontakt mPowiazanie = new Grupa2Kontakt();
                    mPowiazanie.setmKontakt(mKontakt);
                    mPowiazanie.setmGrupa(mGrupaTag);

                    mPowiazanieDAO.create(mPowiazanie);
                }
            }
            finish();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
