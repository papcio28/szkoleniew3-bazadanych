package com.android.szkolenie.szkoleniew3_bazadanych;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import com.android.szkolenie.szkoleniew3_bazadanych.model.Kontakt;
import com.j256.ormlite.android.AndroidConnectionSource;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;
import butterknife.OnItemSelected;


public class MainActivity extends ActionBarActivity {

    public static final String KONTAKT_EXTRA = "param.kontakt";

    @InjectView(R.id.list_view)
    protected ListView mListView;

    private KontaktyAdapter mAdapter;

    // Polacznie do DB
    private ConnectionSource mConnection;
    // Obiekt obslugi tabeli Kontakt
    private Dao<Kontakt, Integer> mDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        // Utworzenie połączenia do bazy danych
        mConnection = new AndroidConnectionSource(new KontaktyOpenHelper(this));
        try {
            // Tworzymy DAO - obiekt dostepu do danych z tabeli Kontakt
            mDAO = DaoManager.createDao(mConnection, Kontakt.class);

            // Przekazujemy liste wszystkich kontaktow z DB do adapter
            mAdapter = new KontaktyAdapter(this, mDAO.queryForAll());
            mListView.setAdapter(mAdapter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAdapter.clear();
        try {
            mAdapter.addAll(mDAO.queryForAll());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @OnItemClick(R.id.list_view)
    public void itemClick(int position) {
        // Pobieramy element kliknietego Kontaktu
        Kontakt mSelected = mAdapter.getItem(position);

        // Otwarcie aktywnoci z przekazaniem ID wybranego kontaktu
        Intent mIntent = new Intent(this, KontaktFormActivity.class);
        mIntent.putExtra(KONTAKT_EXTRA, mSelected.getmID());
        startActivity(mIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_create) {
            // Otwieramy aktywnosc formualrza ale bez ID, w celu utworzeniu
            Intent mIntent = new Intent(this, KontaktFormActivity.class);
            startActivity(mIntent);
        }

        return super.onOptionsItemSelected(item);
    }
}
